<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{        
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;  

	
    /**
	 * @throws PDOException
     */
    public function __construct($db = null)  
    {  
	    if ($db) 
		{
			$this->db = $db;
		}
		else
		{
           	$servername = "localhost";
			$dbname = "assignment 1";
			$username = "root1";
			$password = "root";

			try {
				$this->db = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
				// set the PDO error mode to exception
				$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				}
			catch(PDOException $e)
				{
				echo "Connection failed: " . $e->getMessage();
				} // Create PDO connection
		}
    }
    
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
    {
		$booklist = array();
		$query = $this->db->query('SELECT * FROM book');
		
		while($result = $query->fetch(PDO::FETCH_ASSOC)){
			$books = new Book($result['title'], $result['author'], $result['description'], $result['id']);
			//$books = new Book($result['title'], ['author'], ['description'], ['id']);
			$booklist[] = $books;
			}
		return $booklist;
    }
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {
		if(is_numeric($id)){
		$book = null;
		$query = $this->db->query('SELECT id, title, author, description FROM `book` WHERE id='.$id);
		try {
		$result = $query->fetch(PDO::FETCH_ASSOC);
				$book = new Book($result['title'], $result['author'], $result['description'], $result['id']);
		}catch(PDOException $e) {
			echo "<br>" . $e->getMessage() . "<br>";
		}
        return $book;
		}
    }
    
    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book)
    {
		if(is_numeric($book->id)){
			if(trim($book->title) && trim($book->author)){
				try {
					$sql = $this->db->prepare("INSERT INTO `book`(`title`, `author`, `description`)
							VALUES (:title, :author, :description)");
					$sql->bindParam(':title', $book->title);
					$sql->bindParam(':author', $book->author);
					$sql->bindParam(':description', $book->description);
					$sql->execute();
					
				}catch(PDOException $e){
					echo $sql . "<br>" . $e->getMessage()."<br>";
				}
			}
		}
	}

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {
		if(is_numeric($book->id)){
			if(trim($book->title) && trim($book->author)){
				try {
					$sql = $this->db->prepare("UPDATE `book` SET `title`=:title,`author`=:author,`description`=:description WHERE id =:id");
					$sql->bindParam(':title', $book->title);
					$sql->bindParam(':author', $book->author);
					$sql->bindParam(':description', $book->description);
					$sql->bindParam(':id', $book->id);
					$sql->execute();
					
				}catch(PDOException $e){
					echo $sql . "<br>" . $e->getMessage()."<br>";
				}
			}
		}
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
		try{
			$sql = $this->db->prepare("DELETE FROM `book` WHERE id =:id");
			$sql->bindParam(':id', $id);
			$sql->execute();
			
		}catch(PDOException $e){
		echo $sql . "<br>" . $e->getMessage()."<br>";
		}
    }
	
}

?>